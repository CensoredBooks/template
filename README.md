# Template

A template and tutorial for book projects

Replace this readme with this template:

# Author: Title of the work

Short description

| Book   |   Metadata    |
|----------|:-------------:|
| Title: |  |
| English: | Was Jesus a real person? |
| Author: |  |
| Type: | Novel Essay Poem Theatre ... |
| Subtitle: |  |
| Original title: |  |
| Date:  |  |
| Editor: |  |
| Source of Scans: |  |
| Language: |  |

# Join us in the chat

If you are looking for a book, want to contribute or just want to say hi, feel free to join us:

## Workflow

The process of digitizing a book is not complicated, it is mostly time consuming, you don't need a lot of hardware or a powerful PC either, especially if the book is already digitized.

1. Find the book, you can browse the online catalog of many libraries, local or national, or look on ebay, craigslist, having a copy at home is best because you won't have to take pictures of the book sneakily, the scans will also be of higher quality.
2. Scan all the pages with a camera, smartphone or better, a flatbed scanner, it scans the pages 2-by-2 and the pictures will be aligned and thus a lot easier to process, you can do all the even then odd pages at once or do them in order. A tripod and good lamp can come handy as well.
3. Cropping, renaming and sorting the scans
4. Converting to PDF
5. OCR (optional)
6. Proofreading page per page
7. Merging the proofread .txt in chapters or the full book in markdown
8. Creating a digital edition as well as a searchable, lightweight, full-text PDF
9. Publishing if public-domain
10. Publishing translations (optional)

## Where to look for

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:0f35c59834fbb805c9cfb50a30d9471e?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:0f35c59834fbb805c9cfb50a30d9471e?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:0f35c59834fbb805c9cfb50a30d9471e?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:


## Cropping

xnconvert, gimp, scantailor, gscan2pdf


## Proofreading and OCR

mediawiki proofreadpages plugin
ocr.space


## Publishing

Libgen, Thepiratebay, Archive.org

### Making money ?
